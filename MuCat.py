#!/usr/bin/env python

import sys, pygame
import math
import numpy as np
from random import randint
import random

# You can change the grid size here
grid = (9,9)
# The number of colors
numColors = 5
# You can edit the colors used for pieces
# The first one is used to remove pieces so you shouldn't change it
colors = [
    (0, 0, 0),
    (100, 40, 40, 255),
    (40, 100, 40, 255),
    (40, 40, 100, 255),
    (240, 100, 80, 255),
    (80, 100, 240, 255),
    (80, 240, 100, 255),
    (100, 240, 240, 255),
    (240, 100, 240, 255),
    (240, 240, 100, 255),
]

pygame.init()
gridSize = 40
size = grid[0]*gridSize, grid[1]*gridSize+80
black = 0,0,0
board = np.full(grid, 0, dtype=int)
screen = pygame.display.set_mode(size)
firstClick = False
secondClick = False
animating = False

score = 0

# Initialise the original board layout
def init_board():
    for i in range(grid[0]):
        for j in range(grid[1]):
            board[i][j] = randint(1,numColors)

# Determine which grid square a click is in using x,y coordinates
def get_square():
    pos = pygame.mouse.get_pos()
    y = math.ceil(pos[0]/gridSize)
    x = math.ceil(pos[1]/gridSize)
    return (x-1,y-1)

def get_color(value):
    if value == 0:
        return 0
    else:
        return (value-1)%numColors+1

"""
This draws the item that is at grid coordinates i,j with an optinal vertical offset of k
"""
def draw_item(i,j,k=0):
    colorIndex = get_color(board[i][j])
    pygame.draw.ellipse(screen, colors[colorIndex], (i*gridSize,j*gridSize+k,gridSize,gridSize))
    if board[i][j] > numColors and board[i][j] <= 2*numColors:
        pygame.draw.rect(screen, colors[0], ((i+0.38)*gridSize,j*gridSize+k,gridSize/4,gridSize))
    elif board[i][j] > 2*numColors and board[i][j] <= 3*numColors:
        pygame.draw.rect(screen, colors[0], (i*gridSize,(j+0.38)*gridSize+k,gridSize,gridSize/4))
    elif board[i][j] > 3*numColors and board[i][j] <= 4*numColors:
        pygame.draw.ellipse(screen, colors[0], (i*gridSize,j*gridSize+k,gridSize/2,gridSize/2))
    elif board[i][j] > 4*numColors and board[i][j] <= 5*numColors:
        pygame.draw.ellipse(screen, colors[0], (i*gridSize,j*gridSize+k,gridSize/2,gridSize/2))
    elif board[i][j] > 5*numColors:
        pygame.draw.ellipse(screen, colors[0], (i*gridSize,j*gridSize+k,gridSize/2,gridSize/2))

# draw the screen
def draw_screen():
    if not animating:
        screen.fill(black)
        for i in range(grid[0]):
            for j in range(grid[1]):
                draw_item(i,j)
        if firstClick != False:
            pygame.draw.ellipse(screen, (255,255,255), (firstClick[1]*gridSize,firstClick[0]*gridSize,gridSize,gridSize), 2)
        font = pygame.font.SysFont(None, 40)
        text = font.render("Score: {}".format(score), True, (255,255,255))
        screen.blit(text, [0,(grid[1]+1)*gridSize])
        pygame.display.flip()

# Find every vertical and horizontal run of 3 or more items that are the same type
# it also returns a list of all runs that are longer than 3 or otherwise special
def find_runs():
    runs = set()
    specials = set()
    previous = False
    count = 0
    # count in columns first
    for j in range(grid[1]):
        count = 0
        for i in range(grid[0]):
            if previous == 0:
                previous = False
            if previous == get_color(board[i][j]):
                count += 1
            else:
                if count > 2:
                    # Add the current run to the set
                    for k in range(count):
                        runs.add((i-k-1,j))
                    specials.add((i-1,j,count,get_color(previous), 'column'))
                count = 1
            previous = get_color(board[i][j])
        if count > 2:
            # Add the current run to the set
            for k in range(count):
                runs.add((i-k,j))
            specials.add((i,j,count,get_color(previous), 'column'))
    # then count in rows
    for i in range(grid[0]):
        count = 0
        for j in range(grid[1]):
            if previous == 0:
                previous = False
            if previous == get_color(board[i][j]):
                count += 1
            else:
                if count > 2:
                    # Add the current run to the set
                    for k in range(count):
                        runs.add((i,j-k-1))
                    specials.add((i,j-1,count,get_color(previous), 'row'))
                count = 1
            previous = get_color(board[i][j])
        if count > 2:
            # Add the current run to the set
            for k in range(count):
                runs.add((i,j-k))
            specials.add((i,j,count,get_color(previous), 'row'))
    return runs, specials

def handle_specials(i,j, items):
    itemsToGo = set()
    if board[i][j] > numColors and board[i][j] <= 2*numColors:
        # Remove everything in the column
        for this_j in range(grid[1]):
            if (i,this_j) not in items:
                itemsToGo.add((i,this_j))
    elif board[i][j] > 2*numColors and board[i][j] <= 3*numColors:
        # Remove everything in the row
        for this_i in range(grid[0]):
            if (this_i,j) not in items:
                itemsToGo.add((this_i,j))
    elif board[i][j] > 3*numColors and board[i][j] <= 4*numColors:
        for this_i in range(grid[0]):
            for this_j in range(grid[1]):
                if get_color(board[this_i][this_j]) == get_color(board[i][j]) and ((this_i, this_j) not in items):
                    itemsToGo.add((this_i,this_j))
    if len(itemsToGo) > 0:
        remove_items(itemsToGo | items)

# Remove the pieces listed in the input from the board
# You get points for every removed item
def remove_items(items):
    global score
    for item in items:
        if board[item[0]][item[1]] > numColors:
            handle_specials(item[0],item[1], items)
        board[item[0]][item[1]] = 0
    numCleared = len(items)
    score += 10*numCleared
    if numCleared > 3:
        score += 50
    if numCleared > 4:
        score += 100
    if numCleared > 5:
        score += 200
    if numCleared > 6:
        score += 400


# Make items fall into spaces left by removed items
# Returns a boolean indicating if the falling is finished or not
# Removing pieces should only happen after the falling is finished
def pieces_fall():
    global animating
    done = True
    fallPositions = []
    # we only check columns and move from the bottom up
    # List the lowest empty block in each column, if any
    for i in range(grid[0]):
        column = i
        for j in range(grid[1]-1,0,-1):
            if board[i][j] == 0 and column == i:
                fallPositions.append((i,j))
                column += 1
                done = False
    if len(fallPositions) > 0:
        animate_fall(fallPositions)
    return done

"""
This makes the columns fall down to fill up empty space.
fallPositions is a list of the topmost empty square in each column, if any
all the columns have every square amove the empty one fall down at the same rate
this function animates falling one block, then it will get called again in the
next loop if more falling is needed.
"""
def animate_fall(fallPositions):
    global screen
    global animating
    global board
    # blank out each column above the marked point
    for k in range(gridSize):
        for position in fallPositions:
            columnRect = pygame.Rect(position[0]*gridSize,0,gridSize,(position[1]+1)*gridSize)
            screen.fill((0, 0, 0), columnRect)
            i = position[0]
            for j in range(0,position[1]):
                draw_item(i,j,k)
        pygame.display.update()
        pygame.time.wait(5)
    # Move each of the columns down by one.
    for position in fallPositions:
        i = position[0]
        for j in range(position[1],0,-1):
            board[i][j] = board[i][j-1]
            if j-1 == 0:
                board[i][j-1] = randint(1,numColors)
    animating = False

def add_new_items():
    if not animating or True:
        j = 0
        for i in range(grid[0]):
            if board[i][j] == 0:
                # put a new piece in grid i, j
                board[i][j] = randint(1,numColors)

def accept_move():
    runs, specials = find_runs()
    if len(runs) > 0:
        return True
    else:
        return False

def add_specials(specials):
    for i, j, count, previous, orientation in specials:
        if count > 3:
            if count == 4 and orientation == 'column':
                board[i][j] = previous + numColors
            if count == 4 and orientation == 'row':
                board[i][j] = previous + 2*numColors
            if count >= 5:
                board[i][j] = previous + 3*numColors

init_board()

while 1:
    for event in pygame.event.get():
        if event.type == pygame.QUIT: sys.exit()
        if event.type == pygame.MOUSEBUTTONDOWN:
            if not firstClick:
                firstClick = get_square()
            elif not secondClick:
                secondClick = get_square()
    # If there have been two clicks
    if firstClick != False and secondClick != False:
        # If the two clicks were in adjacent squares
        if (firstClick[0] == secondClick[0] and (firstClick[1]+1 == secondClick[1] or firstClick[1] == secondClick[1]+1)) or (firstClick[1] == secondClick[1] and (firstClick[0]+1 == secondClick[0] or firstClick[0] == secondClick[0]+1)):
            # swap the block in firstClick with the one in secondClick
            square1 = board[firstClick[1]][firstClick[0]]
            square2 = board[secondClick[1]][secondClick[0]]
            board[firstClick[1]][firstClick[0]] = square2
            board[secondClick[1]][secondClick[0]] = square1
            if not accept_move():
                # if the move is not allowed switch the pieces back
                board[firstClick[1]][firstClick[0]] = square1
                board[secondClick[1]][secondClick[0]] = square2
        firstClick = False
        secondClick = False
    finishedFalling = pieces_fall()
    add_new_items()
    if finishedFalling:
        removeThese, specials = find_runs()
        remove_items(removeThese)
        add_specials(specials)
        draw_screen()
