# MuCat

Match 3 thingy, made with PyGame

This requires pygame

To start cd into the repo folder and type ./MuCat.py

To change the board size edit MuCat.py and change the value of grid.

To change how many colors are used edit numColors, if you make it more than 9
you need to add more color definitions.
